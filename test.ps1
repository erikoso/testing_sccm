#Ping test
Function ping-Computers {
#Path for the script
 Set-Location -Path C:/Users/Administrator/testing_sccm/test.ps1

 #Variables
 $PingComputer = Get-Content "client_machines.txt"
 $date = Get-dat -Format "dddd MM/dd/yyyy HH:mm K"

#If an specific external location is used to store log files, use -filepath instead of append and then full path to the log file location
 Write-Output "Start Log" `r | out-file -append replies.log
 Write-Output "Start Log" `r | out-file -append noreplies.log

 #loop for every client machine in the subnet defined in get-content
 ForEach($SystemName In $PingComputer)
  {$Pingstatus = Get-Ciminstance Win32_PingStatus -Filter "Address ='$SystemName'" |
  Select-Object address, StatusCode
  If ($PingStatus.StatusCode -eq 0)
    {Write-Output "$SystemName is alive" |
      out-file -append -filepath replies.log
      }
      Else
      {Write-Output "$SystemName is dead" | 
      out-file -append -filepath noreplies.log
      }
     }
     Write-Output `r "Run on $date" "by env:username at $env:computerName" `r "End log" `r `r | out-file -append -filepath replies.log
     Write-Output `r "Run on $date" "by env:username at $env:computerName" `r "End log" `r `r | out-file -append -filepath noreplies.log
}

Function Get-hostname{
$getHostName = Get-Content "client-machines.txt"
ForEach($HostName In $getHostName)
 {Get-CimInstance -ClassName Win32_ComputerSystem -ComputerName $HostName
 Write-Output 'r "$HostName" | out-file -append -filepath hostname.log
}


Function CCMEEvalReport{
$SendAlways = {
    Param($Value)
    $Path = "HKLM:\Software\Microsoft\CCM\CcmEval"
 
    Try
    {
        $null = New-ItemProperty -Path $Path -Name 'SendAlways' -Value $Value -Force -ErrorAction Stop
    }
    Catch
    {
        $_
    }
}

 $hostname = Get-Content "hostname.log"
 ForEach($hostname In $GetHostName)
      If (!([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
    {
        Write-Warning "This cmdlet must be run as administrator against the local machine!"
        Return
    }
 
    Write-Verbose "Enabling 'SendAlways' in registry"
    $Result = Invoke-Command -ArgumentList "TRUE" -ScriptBlock $SendAlways
 
    If (!$Result.Exception)
    {
        Write-Verbose "Triggering CM Health Evaluation task"
        Invoke-Command -ScriptBlock { schtasks /Run /TN "Microsoft\Configuration Manager\Configuration Manager Health Evaluation" /I }
 
        Write-Verbose "Waiting for ccmeval to finish"
        do {} while (Get-process -Name ccmeval -ErrorAction SilentlyContinue)
 
        Write-Verbose "Disabling 'SendAlways' in registry"
        Invoke-Command -ArgumentList "FALSE" -ScriptBlock $SendAlways
    }
    Else
    {
        Write-Error $($Result.Exception.Message)
    }
}
# Run against remote computer
Else
{
    Write-Verbose "Enabling 'SendAlways' in registry"
    $Result = Invoke-Command -ComputerName $ComputerName -ArgumentList "TRUE" -ScriptBlock $SendAlways
 
    If (!$Result.Exception)
    {
        Write-Verbose "Triggering CM Health Evaluation task"
        Invoke-Command -ComputerName $ComputerName -ScriptBlock { schtasks /Run /TN "Microsoft\Configuration Manager\Configuration Manager Health Evaluation" /I }
 
        Write-Verbose "Waiting for ccmeval to finish"
        do {} while (Get-process -ComputerName $ComputerName -Name ccmeval -ErrorAction SilentlyContinue)
 
        Write-Verbose "Disabling 'SendAlways' in registry"
        Invoke-Command -ComputerName $ComputerName -ArgumentList "FALSE" -ScriptBlock $SendAlways
    }
    Else
    {
        Write-Error $($Result.Exception.Message)
    }
}  
